<?php

namespace Drupal\graphql_poll_vote\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * A poll id.
 *
 * @GraphQLField(
 *   id = "pollid",
 *   secure = true,
 *   name = "pollid",
 *   type = "String!",
  *   parents = {"PollVote"}
 * )
 */
class PollId extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['pollid'];
  }

}
