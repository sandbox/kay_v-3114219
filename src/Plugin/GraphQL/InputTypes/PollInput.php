<?php

namespace Drupal\graphql_poll_vote\Plugin\GraphQL\InputTypes;

use Drupal\graphql\Plugin\GraphQL\InputTypes\InputTypePluginBase;

/**
 * Poll vote input type.
 *
 * @GraphQLInputType(
 *   id = "poll_vote_input",
 *   name = "PollInput",
 *   fields = {
 *     "chid" = "String!",
 *     "pid" = "String!",
 *     "uid" = "String!"
 *   }
 * )
 */
class PollInput extends InputTypePluginBase {

}
