<?php

namespace Drupal\graphql_poll_vote\Plugin\GraphQL\Interfaces;

use Drupal\graphql\Plugin\GraphQL\Interfaces\InterfacePluginBase;

/**
 * PollVote interface definition.
 *
 * @GraphQLInterface(
 *   id = "poll_vote",
 *   name = "PollVote"
 * )
 */
class PollVote extends InterfacePluginBase {

}
