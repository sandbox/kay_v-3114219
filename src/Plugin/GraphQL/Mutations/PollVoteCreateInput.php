<?php

namespace Drupal\graphql_poll_vote\Plugin\GraphQL\Mutations;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Mutations\MutationPluginBase;
use Drupal\graphql_poll_vote\PollVoteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * A test mutation.
 *
 * @GraphQLMutation(
 *   id = "poll_vote_input",
 *   secure = true,
 *   name = "PollVoteCreateInput",
 *   type = "PollVote",
 *   arguments = {
 *     "vote" = "PollInput!"
 *   }
 * )
 */
class PollVoteCreateInput extends MutationPluginBase implements ContainerFactoryPluginInterface {
  use DependencySerializationTrait;

  /**
   * The pollvote.
   *
   * @var \Drupal\graphql_poll_vote\PollVoteInterface
   */
  protected $pollvote;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static($configuration, $pluginId, $pluginDefinition, $container->get('graphql_poll_vote.poll_vote'));
  }

  /**
   * PollVoteCreateInput constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\graphql_poll_vote\PollVoteInterface $pollvote
   *   The pollvote service.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, PollVoteInterface $pollvote) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->pollvote = $pollvote;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve($value, array $args, ResolveContext $context, ResolveInfo $info) {
    return $this->pollvote->insertPollVote($args['vote']);
  }

}
