<?php

namespace Drupal\graphql_poll_vote\Plugin\GraphQL\Types;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * A vote type.
 *
 * @GraphQLType(
 *   id = "vote",
 *   name = "Vote",
 *   interfaces = {"PollVote"},
 * )
 */
class Vote extends TypePluginBase {


}
