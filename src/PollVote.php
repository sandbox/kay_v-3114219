<?php

namespace Drupal\graphql_poll_vote;

/**
 * PollVote implementation.
 *
 * Dummy implementation for the sake of of a complete service definition. To be
 * replaced with prophecies in tests.
 */
class PollVote implements PollVoteInterface {

  /**
   * {@inheritdoc}
   */
  public function insertPollVote(array $poll) {
    $db = \Drupal::database();
    $str = substr(sha1(rand()), 0, 32);
    $test_host = gethostname() . $str;
    $result = $db->insert('poll_vote')
      ->fields(['chid', 'pid', 'uid', 'hostname', 'timestamp'])
      ->values([
        'chid' => $poll['chid'],
        'pid' => $poll['pid'],
        'uid' => $poll['uid'],
        'timestamp' => REQUEST_TIME,
        'hostname' => $test_host,
      ])
      ->execute();

    return NULL;
  }

}
