<?php

namespace Drupal\graphql_poll_vote;

/**
 * Interface definition for a PollVote.
 *
 * Used for testing GraphQL queries and mutations.
 */
interface PollVoteInterface {


  /**
   * Add a poll vote to your Poll.
   *
   * @param array $poll
   *   The poll vote.
   *
   */
  public function insertPollVote(array $poll);


}
